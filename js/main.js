$(function() {

    // Enable buttons
    $(".bt").on("click", function(event) {
        // check for the ladder button
        if (this.id === "pin-ladder") {
            $.post("/", {
                ladder: true
            }, function(data) {
              // console.log(data);
            }, "json");
            return;
        }

        // retrieve pin number from data
        var pin = $(this).data("pin");
        var state = $(this).data("state");

        $.post("/", {
            pin: pin,
            state: state
        }, function(data) {
            // console.log(data);
        }, "json");
    });
});
