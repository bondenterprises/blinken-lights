<html>
<head>
  <title>Raspberry Pi 3 - Control Relay</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .bt{
      font-size: 18pt;
      color: #fdfdfd;
      background-color: #666;
      border-radius: 3px;
      min-width: 120px;
      min-height: 35px;
      line-height: 35px;
      padding: 7px 10px 7px 10px;
      text-align: center;
      text-decoration: none;
      display: inline;
      margin: .5em 0 .5em 0;
    }

    .bt:hover{
      background: #000;
    }

    @media only screen and (max-width:480px) { 
      html *, body * {
        font-size: 16pt;
      }

      .bt {
        width: 100%;
      }
      
      a.bt{
        display: block;
        padding: 0;
      }
    }
  </style>
</head>
<body>
% for pin in pins:
<div>
  <div>PIN #${pin}</div>
  <button id="pin-on-${pin}" data-pin="${pin}" data-state="ON" class="bt">ON</button>
  <button id="pin-off-${pin}" data-pin="${pin}" data-state="OFF" class="bt">OFF</button>
</div>
% endfor
<button id="pin-ladder" class="bt">LADDER</button>

<script src="https://unpkg.com/jquery"></script>
<script src="js/main.js"></script>
</body>
</html>
