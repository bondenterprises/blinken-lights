"""Stub class for GPIO Raspberry Pi library.

Used for local testing so module imports work.

"""


class GPIO:

    STUB = True

    def __init__(self):
        pass

    # def __getattribute__(self, item):
    #     print('__getattribute__: "{}"'.format(item))
    #     return GPIO.default_method

    def __getattribute__(self, item):
        # print('__getattribute__: "{}"'.format(item))
        return GPIO.default_method

    @staticmethod
    def default_method(*args, **kwargs):
        pass
