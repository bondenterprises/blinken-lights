#!/usr/bin/python3
import os
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from json import dumps
from mako.template import Template
from urllib.parse import parse_qs

from RPi import GPIO

# Instantiate the stub class (local development)
if getattr(GPIO, "STUB", False):
    GPIO = GPIO()


# Module parameters
PORT = 8000
PINS = [4, 17, 27, 22]


def init_pins():
    """Initialize the GPIO pins."""

    GPIO.setmode(GPIO.BCM)

    for pin in PINS:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.HIGH)


class PiHandler(BaseHTTPRequestHandler):

    def _send_headers(self, content_type='text/html'):
        self.send_response(200)
        self.send_header('Content-type', content_type)
        self.end_headers()

    def _send_not_found(self):
        """File not found."""
        self.send_response(404)
        self.end_headers()

    @staticmethod
    def _file_contents(filename):
        """Retrieve encoded file contents for response.

        :param str filename: The filename to retrieve/load.
        :returns: None|bytes

        """

        # Get the full path for the file
        cwd = os.path.abspath('.')
        full_path = os.path.join(
            cwd, filename.lstrip(os.path.sep)
        )

        # File not found, caller handle response (possible 404)
        if not os.path.exists(full_path):
            return

        contents = open(full_path, 'r').read()
        return contents.encode('utf-8')

    def _static_resource(self):
        """Handle retrieving a static file from the file system and
        sending the contents back with the appropriate mimetype.

        """

        mime_type_map = {
            '.js': 'application/javascript',
            '.css': 'text/css',
            '.html': 'text/html'
        }

        # Retrieve file contents
        contents = self._file_contents(self.path)

        # Retrieve the path file extension
        ext = os.path.splitext(self.path)[-1]
        mime_type = mime_type_map.get(ext.lower(), 'plain/text')

        if contents is None:
            self._send_not_found()
            return

        self._send_headers(mime_type)
        self.wfile.write(contents)

    @staticmethod
    def _pin_state(pin, state):
        """Set the PIN state.

        :param int pin: The PIN to change state
        :param str state: The state or ON/OFF

        """

        if state == 'ON':
            GPIO.output(pin, GPIO.LOW)
        elif state == 'OFF':
            GPIO.output(pin, GPIO.HIGH)

    def do_HEAD(self):
        self._send_headers()

    def do_GET(self):
        if 'favicon.ico' in self.path:
            self._send_not_found()
            return

        # Handle static files
        ext = os.path.splitext(self.path)[-1]
        if ext.lower() in ['.js', '.css', '.html']:
            self._static_resource()
            return

        # Retrieve the index file
        path = os.path.abspath('.')
        filename = os.path.join(path, 'templates/index.mako')
        index = Template(filename=filename)

        self._send_headers()

        # Render the index template
        data = {'pins': PINS}
        rendered = index.render(**data)
        self.wfile.write(rendered.encode('utf-8'))

    def _handle_ladder(self):
        """Handle the ladder lights."""

        # Ladder the pins ON
        for pin in PINS:
            self._pin_state(pin, "ON")
            time.sleep(0.5)
            
        # Ladder the pins OFF
        for pin in PINS:
            self._pin_state(pin, "OFF")
            time.sleep(0.5)

        response = {
            'success': True,
            'message': 'Ladder successfully executed'
        }

        self._send_headers('application/json')
        self.wfile.write(dumps(response).encode('utf-8'))

    def do_POST(self):
        """Handle post method"""

        try:
            length = int(self.headers['Content-Length'])
            content = self.rfile.read(length)

            # parse query parameters
            params = parse_qs(content.decode('utf-8'))

            ladder = params.get('ladder')
            pin = params.get('pin')
            state = params.get('state')
        except Exception as err:
            print(err)
            ladder = False
            pin = None
            state = None

        if ladder:
            self._handle_ladder()
            return

        if pin is None or state is None:
            response = {
                'success': False,
                'message': 'Missing "pin" or "state".'
            }
        else:
            if isinstance(pin, list) and len(pin) == 1:
                pin = pin[0]
            if isinstance(state, list) and len(state) == 1:
                state = state[0]

            self._pin_state(int(pin), state)

            response = {
                'success': True,
                'message': 'Pin updated successfully',
                'pin': pin,
                'state': state
            }

        self._send_headers('application/json')
        self.wfile.write(dumps(response).encode('utf-8'))


def main():
    # initialize GPIO pins
    init_pins()

    httpd = HTTPServer(('', PORT), PiHandler)
    print('Serving at port: {}"'.format(PORT))

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Exiting')

    GPIO.cleanup()
    httpd.server_close()


if __name__ == '__main__':
    main()
